## 功能模块

### 首页

[http://www.zonsys.com/]()

### 登录页面

[http://sso.zonsys.com/oauth?response_type=token&client_id=gu4ueQl4C0GhSi8Q5odH4g&scope=ABC&state=blablabla&lan=en&redirect_uri=http://coupon.zonsys.com/index.aspx]()

### 几个通用的Ajax

* 获取当前用户信息 [/api/usr.GetCurrentUserJson]()
* 未知 [/api/sec.GetCurUserEffectivePermJson]() 这个请求发送了两次，每次的请求参数不同，好像和用户权限有关

### 后台首页 /ManageredCompany.aspx

模块：

* banner
    * 切换语言
    * 登出
* 公司列表 表格（名称、网站地址、说明、 进入管理界面）
    * 每行hover高亮，单击某行也可以直接进入对应公司的管理界面
    * 可以根据名称、网址、说明排序（客户端排序），同时右侧图片列表会联动
* 图片列表，快速进入，带图，和左侧联动
    * 图片列表，和左侧联动，点击可以进入

Ajax：

* 获取所有可管理公司的信息 [/api/usr.GetManagableAccountsRange?_dc=1489620295543&FromRowIndex=0&RowCount=1000&page=1&start=0&limit=1000]()

建议：

* 目前post请求都是走http header验证，get请求有部分加了验证，get请求返回的都是xml，需要序列化成json，客户端可做，后端统一做最好，可以额外增加一个参数控制输出格式是xml还是json。由于目前是spa的形式，路由在客户端做，所以需要对所有的接口有一个是否已经登录的判断，现在的系统也没有这个功能，需要后端添加。
* 公司列表和图片列表来自同一个数据源，联动展示感觉比较多余，最好把表格的每行增加行高，让公司的logo可以差不多的展示出来，这样一个表格就可以把事情做完。
* 目前所有的公司的列表api没有做分页，可以在现在的基础上增加一个前端的筛选框。

### 公共导航模块

点击某个确定的公司后的页面，以及同级的几个页面都会包含以下模块，统一列出。

模块：

* banner 切换语言，登出，返回到公司管理界面，展示当前用户名
* navigator 导航条，某些有2级目录
    * 点击某个一级目录自动跳转到与之相关的第一个二级目录
    * 一级目录文字hover时变色，选中时变色加粗，二级目录文字选中时加粗
* 所有导航内容
    * 首页
    * 优惠券
        * 优惠券分组管理
    * 订单
        * 订单管理
    * 账户
        * 公司管理
        * 用户管理
    * 产品
        * 产品管理
        * 类别管理
    * 客户
        * 客户管理
        * 客户组管理
    * 报表
        * 优惠券分发报表
        * 订单报表
    * 设置
        * 产品管理
        * 类别管理

建议：

* `设置`和`产品`两个超链接的地址是一样的，是否是bug还是功能重叠，可以保留一个
* 目前整体宽度是固定940px，可以搞成百分比宽度，这样可以根据用户的分辨率自适应，应该可以展示更多的内容

### 公司管理首页 /dashboard/Dashboard.aspx

模块：

* 左上角黄色 `已推送` `已收藏`
* 左下方蓝色 `折扣信息`
* 中间图表 `三个图表`，优惠券收藏、优惠券使用、优惠券浏览，可以根据时间过滤数据，提供“今天、昨天、过去7天、本月、上个月”快捷设置时间，也可以自定义时间
* 右侧蓝色 `优惠券相关整体数据`

Ajax：

* 获取优惠券的整体信息，分别给左侧和右侧使用 [/api/coup.GetCouponDashboardJson?AccountId=15736&Recursive=true]()
* 获取优惠券使用数据 [/api/coup.GetCouponUsedCart?_dc=1489623860876&AccountId=15736&Recursive=true&startTime=2017-03-09T00%3A00%3A00&endTime=2017-03-16T00%3A00%3A00&page=1&start=0&limit=10000]()
* 获取优惠券浏览数据 [/api/coup.GetCouponViewedCart?_dc=1489623860879&AccountId=15736&Recursive=true&startTime=2017-03-09T00%3A00%3A00&endTime=2017-03-16T00%3A00%3A00&page=1&start=0&limit=10000]()
* 获取优惠券收藏数据 [/api/coup.GetCouponCollectedCart?_dc=1489623860880&AccountId=15736&Recursive=true&startTime=2017-03-09T00%3A00%3A00&endTime=2017-03-16T00%3A00%3A00&page=1&start=0&limit=10000]()

问题：

* 左侧下方这个panel是用来展示销售量最好的优惠券么？最多只显示3个还是根据返回的数据都展示出来
* 三个图表中的Y轴分别代表什么意思？

建议：

* 这几个模块的信息权重是怎样的？现在几个模块基本是平均分割界面，我觉得可以让图表的空间再大一些。

### 优惠券

#### 优惠券组管理 /coupon/CouponGroup.aspx

模块：

* 优惠券组表格
    * 过滤和检索 根据公司名称，起止时间
    * 展示组名、公司名、折扣方式、优惠券格式、开放时间和结束时间
    * 每行hover和选中颜色会高亮
    * 双击每行，进入其包含的折扣列表
    * 选中一行，可以编辑、删除、查看和发送。

Ajax：

* 获取带有地址的组信息 [/api/usr.GetAccountsExRangeWithAddr?_dc=1489629147301&RootAccountId=15736&Recursive=true&FromRowIndex=0&RowCount=1000000000&AddressLabel=default&page=1&start=0&limit=25]()
* 获取所有的优惠券分组 [/api/coup.GetCouponGroupsRange?_dc=1489629147610&AccountId=15736&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()
* 获取客户组信息 [/api/coup.GetCustomerGroupsRange?_dc=1489629147614&AccountId=15736&Recursive=false&FromRowIndex=0&RowCount=1000000&page=1&start=0&limit=25]()

问题：

* 列表中的`下载`按钮没有效果
* 优惠券组中的`发送`是什么意思
* Group的描述信息和所包含的具体某个商品的折扣信息有冲突的话，Group的信息参考价值就不大了。比如，所包含的折扣不都是Percent的话，那么这个表格中的“折扣方式”一列还有必要存在么，其他列同理。
* 这三个ajax请求分别对应哪里的数据？看界面很多数据并没有用到

#### 优惠券列表 /coupon/CouponList.aspx?gid=15783

模块：

* 优惠券列表
    * 根据开放和结束时间检索
    * 两种展现方式，列表和图表。切换到图表展现时，操作按钮都隐藏。
    * 展示优惠券名称、折扣方式、公司、折扣、价格、开始时间、结束时间、状态
    * 列表展示状态下，每行hover高亮，单击某行高亮并选中，双击某行弹出预览，hover到“查看”可以预览移动端效果。图表状态下，单击弹出预览。

Ajax:

* 获取带有地址的组信息 [/api/usr.GetAccountsExRangeWithAddr?_dc=1489637523252&RootAccountId=15736&Recursive=true&FromRowIndex=0&RowCount=1000000000&AddressLabel=default&page=1&start=0&limit=25]()
* 获取优惠券列表 [/api/coup.GetCouponsRange?_dc=1489637523655&GroupId=15783&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()
* 获取优惠券对应的分组 [/api/coup.GetCouponGroupsByIdJson]()
* 获取某个优惠券的详细信息 [/api/coup.GetCouponsByIdWithDetailJson]() 表格中的“查看”按钮
* 发布优惠券[/api/coup.PublishCouponsToWeb]() 发布按钮

问题：

* 优惠券表格中的提交、暂停和发布时什么意思？
* 列表中两种方式切换的时候，都是同一个请求地址。不过一个是json，一个是xml。这是何意？
* 创建完无法修改是什么问题，“只能修改草稿和拒绝状态的优惠券”

建议：

* 列表中的“查看”应该叫“预览”

#### 增加优惠券 /coupon/AddCoupon.aspx?gid=15783

模块：

* 快速添加 自己手动添加各类数据
    * 图片选择
* 批量添加 在现有商品列表中导入部分数据，加工以后再添加
    * 现存产品列表 目前已有产品包括名称、图片、价格
        * 每行hover高亮，单击弹出编辑Dialog
        * 添加到“已选择”产品列表中的，点击没有反应
    * 已选择产品列表 对已增加的优惠券进行管理
        * 删除和修改优惠券信息
* 右侧预览
      * 手机app预览
      * 邮件二维码预览

Ajax：

* 获取优惠券对应的分组 [/api/coup.GetCouponGroupsByIdJson]()
* 未知 [/api/usr.GetAccountsExRangeWithAddr?_dc=1489655809431&RootAccountId=15736&Recursive=true&FromRowIndex=0&RowCount=1000000000&AddressLabel=default&page=1&start=0&limit=25]()
* 获取当前公司的产品[/api/prod.GetProductsRange?_dc=1489655820570&AccountId=15736&Recursive=true&FromRowIndex=0&RowCount=1000000&MinHintIdx=0&MinHintId=0&MaxHintIdx=0&MaxHintId=0&StatusSearch=R&page=1&start=0&limit=25]()
* 检查优惠券名称是否存在 [/api/coup.CheckCouponNameJson]()
* 未知 [/service/CouponService.asmx/CouponImg]()
* 提交优惠券数据 [/api/coup.CreateCouponsJson]()

问题：

* 代金券和优惠券的区别在哪里？
* 百分比的代金券无法预览
* 优惠券添加了再删除，再重新添加的时候，会显示名称存在
* 快速添加的时候，右侧的邮件模板没有办法预览
* 添加时的预览和在列表中的预览效果不一致

### 订单

#### 订单列表 /campaign/GetOrders.aspx

模块：

* 订单列表 展示订单信息
    * 根据公司名称、起止时间和状态信息进行过滤
    * 列表展示用户名、公司名、购买数、价格、状态、描述和创建时间
    * 每行hover高亮，单击可以高亮并选中，双击可以查看订单详情

Ajax：

* 未知 [/api/usr.GetAccountsExRangeJson]()
* 订单列表 [/api/prod.GetOrdersRange?_dc=1489660279952&AccountId=15736&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()

问题：

* 上面筛选那里，公司选项里面为能有一个公司，理论上不就只能是自己的公司么？还有这个输入框，在输入字符进行suggest的时候，会报500错误，[/api/prod.GetOrdersRange?_dc=1489659976245&AccountId=se&Recursive=0&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()是这个请求

建议：

* 列表第一列没有checkbox，和其他列表不一致，最好都带上。

#### 订单详情 /campaign/GetOrderItemRange.aspx

模块：

* 订单详细列表
    * 包含订单号和订单购买的产品、数量和价格
    * 列表和图表两种展示模式
    * 每行hover高亮，单击高亮选中，双击弹窗可以查看产品的信息

Ajax：

* 未知 [/api/usr.GetAccountsExRangeJson]()
* 获取订单详细信息 [/api/bil.GetOrderItemsRange?_dc=1489661072714&OrderId=20942&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]() 
* 获取产品信息 [/api/prod.GetObjectsById]()

建议：

* 订单详情中一般不会包含太多的商品，可以不需要表格展示，就使用普通的列表，这样切换列表和图表的时候，也好布局。

### 账户

#### 公司管理 /user/companylist.aspx

模块：

* 公司列表
    * 可以根据公司状态筛选
    * 展示公司名称、网址、公司类型、主公司名称、地址、电话、状态、创建时间
    * 每行hover高亮，单击高亮选中，双击跳转到公司编辑页面
    * 添加公司跳转到添加页面
    * 编辑到添加页面，状态是编辑，查看也是到添加页面，但只能查看

Ajax：

* 获取公司列表 [/api/usr.GetAccountsExRange?_dc=1489662414908&RootAccountId=15736&Recursive=true&FromRowIndex=0&AddressLabel=Default&RowCount=25&MinHintIdx=0&MinHintId=0&MaxHintIdx=0&MaxHintId=0&page=1&start=0&limit=25]()

问题：

* 删除是灰色的为何？和公司类型有关系么？

#### 公司添加 /user/Company.aspx?o=1

编辑和查看也是到这个页面 querystring分别是

* e=U&o=2&id=15736
* e=R&o=2&id=15736

模块：

* 公司信息
    * 图片上传后显示在缩略图里面，走的是iframe上传，上传后显示一个json结果
    * 类别可以多选，但是无法自己添加
    * 公司创建完毕跳转到产品管理界面
* 公司地址
    * 地址填写后可以自动在下面定位到定位到所在城市
* 谷歌地图
    * 地图通过地址信息来展示所在位置

Ajax：

* 获取类别 [/api/usr.GetAccountCategories?Language=zh-CHS&_dc=1489697592053&page=1&start=0&limit=1000]()
* 获取国家数据 [/api/usr.GetCountryRange?_dc=1489697592057&Language=zh-CHS&page=1&start=0&limit=1000]()
* 获取账户相关的公司信息 [/api/usr.GetAccountsExRangeJson]() 获取公司和子公司的信息 编辑、查看时请求
* 上传图片[/account/UploadImg.aspx]()
* 创建公司[/api/usr.CreateAccountsJson]()
* 获取公司额外的信息 [/api/usr.GetAccountsExInfoById]() 选中的类别和图片数据 编辑、查看时请求
* 获取公司的地址信息 [/api/usr.GetAccountAddressesJson]() 编辑、查看时请求

问题：

* [/api/usr.GetAccountCategories]()和[/api/usr.GetCountryRange]() 这两个url都被请求了两次
* 公司图片上传后预览效果，整个流程很慢
* 创建的公司，如果不是当前公司的子公司，那么在当前的公司列表中看不到，这个权限只有root用户有么？其他公司的管理员是不是只能创建本公司的子公司？

#### 用户管理 user/UserInfo.aspx

模块：

* 用户列表
    * 展示用户的昵称、姓名、邮箱、电话、地址、性别、公司名称信息
    * 每行hover高亮，单击选中，双击进入查看界面

Ajax：

* 获取所有的用户信息 [/api/usr.GetUsersExRange?_dc=1489705875773&RootAccountId=15736&Recursive=true&FromRowIndex=0&RowCount=25&AddressLabel=Default&MinHintIdx=0&MinHintId=0&MaxHintIdx=0&MaxHintId=0&page=1&start=0&limit=25]()
* 删除用户 [/api//usr.DeleteUsersJson]()


问题：

* 这些用户是做什么用的

#### 添加用户 user/InsertUser.aspx?o=1

编辑和查看用户也是这个地址，url不同

* 编辑 /user/InsertUser.aspx?e=U&o=2&id=15519
* 查看 /user/InsertUser.aspx?e=R&o=2&id=15519

模块：

* 基本信息
    * 照片略缩图这部分可以做成通用的模块
* 地址信息
    * 和添加公司的那个地址信息结构一样，不需要验证，也不需要和google map联动

Ajax：

* 获取账户相关的公司信息 [/api/usr.GetAccountsExRangeJson]()
* 获取用户名是否已存在 [http://sso.zonsys.com/chkusername?username=%E5%A4%A7%E8%A1%A8%E5%93%A5]()
* 获取email是否存在 [http://sso.zonsys.com/chkemail?email=dabiaoge@gmai.com]()
* 创建用户 [/api/usr.CreateUsersJson]()
* 获取用户信息 [/api/usr.GetUsersExByIdJson]() 编辑和查看时请求
* 获取用户的地址信息 [/api/usr.GetUserAddressesJson]() 编辑和查看时请求

问题：

* 判断用户名存在的接口，返回值最好统一，现在直接返回了一段文本。sso这几个请求都收jsonp的，是还有其他的系统在用么？
* 创建用户接口现在返回的是500错误

建议：

* 用户的个人信息和地址信息作为一个接口返回

### 产品

#### 产品管理 setting/ProductList.aspx

模块：

* 产品列表
    * 检索栏可以根据公司名称、产品起止时间和状态进行过滤
    * 列表展示公司名称、产品名称、描述、价格、类别、子类、开始时间、结束时间和状态
    * 行行为同其他表格,双击是查看产品
    * 添加、修改、查看都是打开编辑页面，状态不同而已


Ajax：

* 获取当前公司的所有产品 [/api/prod.GetProductsRange?_dc=1489708643222&AccountId=15284&Recursive=true&FromRowIndex=0&RowCount=25&MinHintIdx=0&MinHintId=0&MaxHintIdx=0&MaxHintId=0&page=1&start=0&limit=25]() 可以根据关键字查询
* 删除所选产品 [/api/prod.DeleteProductsJson]() 数组的形式来删除
* 提交所选 [/api/prod.SubmitProductsJson]()

问题：

* 图表展示的时候，403错误
* 从草稿到待审，谁去审核，入口在哪里？

建议：

* 检索里面的产品名称不支持模糊搜索，最好支持

#### 添加产品 setting/UpdateAsset.aspx

编辑和查看也是到这个地址，url不同

模块：

* 模板导入多个产品
    * 按照公司名称和类型来导入
* 手动导入一个产品
* excel导入多个产品

Ajax：

* 获取所有的产品类型 [/api/tpl.GetTemplateTagsRange]()
* 根据产品id获取产品的信息 [/api/prod.GetProductsByIdJson]() 编辑查看时请求
* 获取类别 [/prod.GetCategories?_dc=1489710920288&AccountId=15284&Source=2&Language=zh-CHS&page=1&start=0&limit=1000]() 获取大类和子类，都是通过这个api，参数不同 编辑查看时请求

问题：

* 模板导入多个产品，现在报js错误，不能用
* 子类别的数据是根据前面的大类来选择的么？
* [/api/tpl.GetTemplateTagsRange]() 在创建页面被请求了2次，querystring有差异，一个带AccountIds
* [/prod.GetCategories]() 在编辑和查看页面分别被请求了4次，也就是每个类别都被请求了2次。
* 创建页面创建一个产品保存的话就直接变成上线状态了，搜索里面那么多的状态都是怎么来的？
* 上传图片时的“平板尺寸”勾选的话，会有什么效果？

建议：

* excel导入过个产品的功能暂时隐藏，这种字段内容比较多，比较复杂的excel，很容易出现结构不一致导致导入失败，效率反而不高。而且，用户可以也会从自己的现有数据来录入这个excel，整体成本和one by one的导入成本差不多。

#### 类别管理 /setting/Category.aspx

模块：

* 类别列表
    * 展示所有的类别，可以创建二级类别
    * 双击每行是查看类别

Ajax：

* 获取所有类别 [/api/prod.GetCategoriesRange?_dc=1489713138410&AccountId=15284&Recursive=1&FromRowIndex=0&RowCount=25&Language=zh-CHS&page=1&start=0&limit=25]()
* 根据id获取某个类别详情 [/api/prod.GetCategoriesById]()
* 删除所选择的类别 [/api/prod.DeleteCategories]()
* 更新某个类别 [/api/prod.UpdateCategories]()

问题：

建议：

* 这个列表里面，直接再把图片显示出来，不需要单独的查看按钮了，因为只有这几个字段信息，在表格中就可以展示。

### 客户

#### 客户管理 /setting/CustomerList.aspx

模块：

* 客户列表
    * 展示客户的姓名、手机号、邮箱、描述和公司
    * 修改和查看都是弹框展示
* 发送优惠券
    * 可以发送把某些优惠券组的信息以邮件或者短信的形式发送给客户
* 导入客户
    * 通过excel导入客户信息
* 创建表单
    * 客户基本信息，姓名、公司、手机号、邮箱、描述
    * 扩展信息，性别、年龄、爱好


Ajax：

* 发送emial [/service/CouponService.asmx/SendEmail2]()
* 获取所有的客户信息 [/api/coup.GetCustomersRange?_dc=1489714984323&AccountId=15284&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()
* 获取所有的优惠券组的信息 [/api/coup.GetSendCouponGroupsRange?_dc=1489714984327&AccountId=15284&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()
* 获取兴趣列表 [/api/coup.GetHobbies]() 
* 删除选中的客户 [/api/coup.DeleteCustomersJson]()

问题：

* 发送优惠券的这个功能并不能用。需要沟通一下这个功能

#### 客户组管理 /setting/CustomerGroupList.aspx

模块：

* 客户组列表
    * 展示客户组名称、描述和所在公司
    * 添加、修改、查看都是弹框形式
    * 行行为同其他表格，双击查看组信息
* 创建客户组
    * 增加客户组的名称和公司
    * 根据所在公司分配对应的客户

Ajax：

* 获取所有客户组信息 [/api/coup.GetCustomerGroupsRange?_dc=1489716431178&AccountId=15284&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()
* 获取所有的客户信息 [/api/coup.GetCustomersRange?_dc=1489716430349&AccountId=15284&Recursive=true&FromRowIndex=0&RowCount=1000000000&page=1&start=0&limit=1000]()
* 未知 [/api/coup.RemoveCustomersFromGroupsJson]()
* 给客户组添加客户 [/api/coup.AddCustomersToGroupsJson]()
* 删除客户组 [/api/coup.DeleteCustomerGroupsJson]()

问题：

* 添加客户 [/api/coup.AddCustomersToGroupsJson]() 这个请求一直是报错的

建议：

* 现在创建客户组的时候，公司名称和所包含的员工是分开的，可以统一到一个表单里面。
* 查看客户组信息的面板，只显示当前客户组包含的客户即可，不需要展示可选的客户列表。

### 报表

#### 优惠券分发报表 /report/CouponDistributed.aspx

模块：

* 分发列表
    * 可以根据主公司子公司、优惠券分组、起止时间来检索
    * 列表展示优惠券名称、PV、发送个数、收藏个数和使用个数

Ajax：

* 获取优惠券分发列表 [http://coupon.zonsys.com/api/coup.GetCouponsReport?_dc=1489720127954&AccountId=15736&Recursive=true&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()

问题：

* 检索里面的“优惠券组”这个select获取不到数据
* 检索里面的“起止时间”是优惠券的起止时间么？

#### 订单报表 /report/FinancialReport.aspx

模块：

* 订单列表
    * 根据主公司和子公司、订单的起止时间进行检索

Ajax：

* 获取订单列表 [/api/prod.GetOrdersRange?_dc=1489720294735&AccountId=15736&FromRowIndex=0&RowCount=25&page=1&start=0&limit=25]()

问题：

* 订单列表中的倒数第二列`描述`是在哪里添加的，手机客户端么？

建议：

* 这个列表并没有删除、编辑等其他操作，所以第一列的checkbox就不需要了。


### 密码忘记

[http://sso.zonsys.com/c.gu4ueQl4C0GhSi8Q5odH4g/reset?response_type=token&client_id=gu4ueQl4C0GhSi8Q5odH4g&language=en-US&redirect_uri=http://coupon.zonsys.com/index.aspx&scope=ABC]()

### 登录

[http://sso.zonsys.com/c.gu4ueQl4C0GhSi8Q5odH4g/register?response_type=token&client_id=gu4ueQl4C0GhSi8Q5odH4g&language=en-US&redirect_uri=http://coupon.zonsys.com/index.aspx&scope=ABC]()

## 公共的一些建议

* 所有的表格的排序，都在客户端做的，意义不大，建议去掉没用的，只保留有意义的排序。
* 表格中存在列表和图表切换的，只要是数据没有变化的，不需要单独请求，只需要展示层改变一下即可。
* 表格中的修改、删除、查看等操作，可以放到表格的一个列中，这样不用先选中某行再操作，直接在当前行就能操作，缺点就是对于多个列的表格，可能数据展示的空间就更少了。
* 表格中的查看，可以单出做成一个弹窗，而不是都跳转到新建里面去查看
* 表格中的排序，直接点击某列都是客户端排序。排序后再点击“搜索”一类的按钮，才是服务器端排序。这个需要讨论下。
* 很多页面有非常多的ajax请求，个别的有8-10个，非常慢，需要梳理
* 现在的session没有设置过期时间，如果用户不关闭浏览器就一直存在，一方面增加了服务器负担，另外一方面可能会引起安全问题，最好给session设置一个过期时间。
* 很多get请求里面的参数都没用，去掉好么？
* 对于hobby和country信息这种数据，不是动态管理的，可以只在第一次请求的时候获取并在客户端cache，以后再获取就直接从内存里面拿，而且这个之和session有关，用户下次登录的话，就需要再重新获取一次。
